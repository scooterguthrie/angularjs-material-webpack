const path = require("path");
const webpack = require("webpack");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Directory Variables
const srcRootDir = path.join(__dirname, "src"); // same as "./src"
const distDir = path.join(__dirname, "dist"); // same as "./dist"



module.exports = {
    //devtool: 'inline-source-map', // Adds sourcemaps to the bundled JS file
    devtool: 'source-map', // Creates external sourcemap files

    context: srcRootDir, // Set the root for your entry files

    entry: { // Select the entry file(s)
        app: "./index.js" // same as "./src/index.js" because it is prefixed with the "context" path
    },

    output: {
        path: distDir,
        filename: "scripts/[name].bundle.js"
    },

    module: {
        rules: [{
                test: /\.html$/,
                include: /ui/,
                use: "file-loader?name=[path][name].[ext]"
            },
            {
                test: /\.(scss|css)$/,
                include: /src/,
                // External CSS with sourcemaps
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        "css-loader?sourceMap",
                        "postcss-loader?sourceMap",
                        "sass-loader?sourceMap"
                    ],
 					publicPath: "../" // Because we are moving the CSS into a folder, the publicPath has to be set to update all urls (images, fonts, svgs) with "../"
                })
                // Bundled CSS with js (sourcemaps not supported when bundled with js)
                // use: [
                //     "style-loader",
                //     "css-loader",
                //     "sass-loader"
                // ]
            },
            {
                test: /\.(jp?g|png|gif|tiff|bmp|svg)$/,
                include: /images/,
                use: "file-loader?name=./images/[name].[ext]"
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                include: /fonts/,
                use: "file-loader?name=./fonts/[name].[ext]"
            },
            {
                test: /\.(js|jsx)$/,
                include: /src/,
                use: "babel-loader"
            },
        ],
    },

    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: [distDir]
            }
        }),
        new CleanWebpackPlugin(['dist'], {
            verbose: true,
            dry: false
        }),
        new ExtractTextPlugin({
            filename: "./styles/[name].bundle.css",
            allChunks: true
        }),

        // HMTL Pages
        new HtmlWebpackPlugin({
            title: "AngularJS, Material, and Webpack",
            template: srcRootDir + "/index.template.html", // Load a custom template
            //chunks: ["app"] // "entry" chuncks
        })
    ]

};
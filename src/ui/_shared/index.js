// Templates
require("./templates/main/main.template.html");
require("./templates/main/main.controller.js");

// Main Nav
require("./main-navbar/main-navbar.template.html");

// Footer
require("./footer/footer.template.html");
require("./footer/footer.controller.js");